package org.example;

public class Main {
    public static void main(String[] args) {
        Product lada = new Product("Лада", 5000, 1.33);
        Product mercedes = new Product("Мерс", 10000000, 4.3);
        Product bmw = new Product("БВМ", 9500000, 4.22);

        Category cars = new Category("Машины", new Product[]{lada, mercedes, bmw});

        Product vodka = new Product("Водка", 320, 9.99);
        Product water = new Product("Вода", 30, 1.0);

        Category products = new Category("Продукты", new Product[]{vodka, water});

        Basket basket = new Basket(new Product[]{mercedes, vodka});
        User user = new User("User", "qwerty", basket);
    }
}