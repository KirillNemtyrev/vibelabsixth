package org.example;

public class Product {
    private String name;
    private int cost;
    private double rating;

    public Product(String name, int cost, double rating) {
        this.name = name;
        this.cost = cost;
        this.rating = rating;
    }
}
